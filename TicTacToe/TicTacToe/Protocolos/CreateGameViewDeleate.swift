import UIKit

protocol CreateGameViewDelegate {
    func refreshTags(index: Int)
}
