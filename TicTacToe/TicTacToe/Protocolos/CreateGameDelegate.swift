import UIKit

protocol CreateGameDelegate {
    func addGame(game: CellContent)
    func exitCreateGameMenu()
}
