import UIKit

class CreateGameView: UIView {

    @IBOutlet weak var nameOfTheGameLabel: UILabel!
    @IBOutlet weak var exitCreateGameView: UIButton!
    @IBOutlet weak var createGameButton: UIButton!
    @IBOutlet weak var gameNameTextField: UITextField!
    @IBOutlet weak var addGameNameButton: UIButton!
    @IBOutlet weak var addTagsButton: UIButton!
    @IBOutlet weak var tagTextField: UITextField!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    
    var cellContent = CellContent(gameName: "no name", tags: [])
    var delegate:CreateGameDelegate?
    
    class func instanceFromNib() -> CreateGameView {
        return UINib(nibName: "CreateGameView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CreateGameView
    }
    
    @IBAction func addGameNameButtonIsPressed(_ sender: UIButton) {
        guard let gameName = gameNameTextField.text, !gameName.isEmpty else {
            return
        }
        cellContent.gameName = gameName
        nameOfTheGameLabel.text = gameName
        gameNameTextField.text = ""
     }
    
    @IBAction func addTagsButtonIsPressed(_ sender: UIButton) {
        if let tag = tagTextField.text, !tag.isEmpty {
            cellContent.tags.append(tag)
        }
        tagsCollectionView.reloadData()
        tagTextField.text = nil
    }
    
    @IBAction func createGameButtonIsPressed(_ sender: UIButton) {
        delegate?.addGame(game: cellContent)
        clearXib()
    }
    
    @IBAction func exitCreateGameViewButtonIsPressed(_ sender: UIButton) {
        delegate?.exitCreateGameMenu()
        clearXib()
    }
    
    func clearXib() {
        refreshCellContentModel()
        refreshNameOfTheGameLabel()
        tagsCollectionView.reloadData()
    }
    
    func refreshCellContentModel() {
        cellContent.gameName = "no name"
        cellContent.tags = []
    }
    
    func refreshNameOfTheGameLabel() {
        nameOfTheGameLabel.text = "Enter the name of the game"
    }
    
}

extension CreateGameView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellContent.tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCreationCollectionViewCell", for: indexPath) as? TagCreationCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.delegate = self as CreateGameViewDelegate
        cell.index = indexPath.row
        cell.configCellUI(with: collectionView)
        cell.configCell(with: cellContent.tags[indexPath.row])
        return cell
    }
    
}

extension CreateGameView: CreateGameViewDelegate {
    
    func refreshTags(index: Int) {
        cellContent.tags.remove(at: index)
        tagsCollectionView.reloadData()
    }
    
}
