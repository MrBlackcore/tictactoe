import UIKit

class TagCreationCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var tagNameLabel: UILabel!
    @IBOutlet weak var deleteTagButton: UIButton!
    
    var index:Int?
    var delegate:CreateGameViewDelegate?

    @IBAction func deleteTagButtonIsPressed(_ sender: UIButton) {
        if let text = tagNameLabel.text, !text.isEmpty {
            if let delegate = delegate, let index = index {
                delegate.refreshTags(index: index)
            }
        }
    }
    
    func configCell(with text: String) {
        tagNameLabel.text = text
    }
    
    func configCellUI(with collectionView: UICollectionView) {
        deleteTagButton.layer.cornerRadius = frame.size.height/2
        layer.cornerRadius = frame.size.height/2
    }
    
}
