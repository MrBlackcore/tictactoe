import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var tagLabelContainerView: UIView!
    
    func configCell(with tag: String) {
        tagLabel.text = tag
        configTagLabelContainer()
    }
    
    func configTagLabelContainer() {
        tagLabelContainerView.layer.cornerRadius = tagLabel.font.pointSize/2
        tagLabelContainerView.backgroundColor = .white
    }
    
}
