import UIKit
import AlignedCollectionViewFlowLayout

class GameCell: UICollectionViewCell {
    
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    
    var tags:[String] = []
    
    override func layoutSubviews() {
        let alignedFlowLayout = tagsCollectionView.collectionViewLayout as? AlignedCollectionViewFlowLayout
        alignedFlowLayout?.horizontalAlignment = .left
        alignedFlowLayout?.verticalAlignment = .top
    }
    
    func configCell(with model: CellContent) {
        setUpUI()
        gameNameLabel.text = model.gameName
        tags = model.tags
    }
    
    private func setUpUI() {
        self.contentView.layer.cornerRadius = 10
    }
    
}

extension GameCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as? TagCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configCell(with: tags[indexPath.row])
        return cell
    }
    
}
