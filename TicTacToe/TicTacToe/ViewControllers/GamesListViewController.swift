import UIKit
import SocketIO
import AlignedCollectionViewFlowLayout

class GamesListViewController: UIViewController {

    @IBOutlet weak var gamesLabel: UILabel!
    @IBOutlet weak var createGameButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tagSearchBar: UISearchBar!
    
    //
    var searchTags = [String]()
    var isSearching = false
    //
    
    var gamesList:[CellContent] = []
    
    //
    var searchedGameList:[CellContent] = []
    //
    
    lazy var createGameXib = CreateGameView.instanceFromNib()
    
    //MARK: - Main functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adaptFont()
        fillInCellCOntent()
        tagSearchBar.autocapitalizationType = .none
    }
    
    func fillInCellCOntent() {
        let first = CellContent(gameName: "test", tags: ["wow", "try", "just"])
        let second = CellContent(gameName: "test2", tags: ["great", "best"])
        let third = CellContent(gameName: "test44444", tags: ["nice", "done", "more", "lol", "ok"])
        gamesList.append(first)
        gamesList.append(second)
        gamesList.append(third)
    }
    
    //MARK: - IBActions

    @IBAction func createGameButtonIsPressed(_ sender: UIButton) {
        showCreateGameXib()
        createGameButton.isUserInteractionEnabled = false
    }
    
    //MARK: - Flow functions
    
    private func adaptFont() {
        gamesLabel.adaptFontSize(to: gamesLabel.frame.size.height)
        createGameButton.titleLabel!.adaptFontSize(to: createGameButton.frame.size.height)
    }
    
    private func showCreateGameXib() {
        let widthAndHeight = self.view.frame.size.width*0.8
        let x = self.view.frame.size.width/10
        let y = (self.view.frame.size.height - widthAndHeight)/2
        createGameXib.frame = CGRect(x: x, y: self.view.frame.size.height, width: widthAndHeight, height: widthAndHeight)
        createGameXib.delegate = self
        createGameXib.tagsCollectionView.register(UINib(nibName: "TagCreationCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TagCreationCollectionViewCell")
        let alignedFlowLayout = createGameXib.tagsCollectionView.collectionViewLayout as? AlignedCollectionViewFlowLayout
        alignedFlowLayout?.horizontalAlignment = .left
        alignedFlowLayout?.verticalAlignment = .top
        self.view.addSubview(createGameXib)
        animateCreateGameXib(y: y)
    }
    
    func animateCreateGameXib(y: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            self.createGameXib.frame.origin.y = y
        }
    }
    
}

extension GamesListViewController: CreateGameDelegate {
    
    func addGame(game: CellContent) {
        gamesList.append(game)
        collectionView.reloadData()
        createGameButton.isUserInteractionEnabled = true
        animateCreateGameXib(y: self.view.frame.size.height)
    }
    
    func exitCreateGameMenu() {
        animateCreateGameXib(y: self.view.frame.size.height)
        createGameXib.removeFromSuperview()
        createGameButton.isUserInteractionEnabled = true
    }
    
}

extension GamesListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.size.width/2 - 5
        let height = self.view.frame.size.width/2 - 5
        let size = CGSize(width: width, height: height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearching {
            return searchedGameList.count
        } else {
            return gamesList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCell", for: indexPath) as? GameCell else {
            return UICollectionViewCell()
        }
        if isSearching {
            cell.configCell(with: searchedGameList[indexPath.row])
        } else {
            cell.configCell(with: gamesList[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController {
            viewController.modalPresentationStyle = .fullScreen
            viewController.modalTransitionStyle = .flipHorizontal
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
}

extension GamesListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        guard !searchText.isEmpty else {
//            searchedGameList = gamesList
//            retu
//        }
        
        isSearching = true
        searchedGameList = []
        for game in gamesList {
            let gameTags = game.tags
            searchTags = gameTags.filter({$0.prefix(searchText.count) == searchText})
            if searchTags.count > 0 {
                searchedGameList.append(game)
            }
        }
        collectionView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearching = false
        collectionView.reloadData()
    }
    
}

