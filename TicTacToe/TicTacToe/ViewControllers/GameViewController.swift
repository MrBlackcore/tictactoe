import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var currentMoveLabel: UILabel!
    @IBOutlet var optionButtons: [UIButton]!
    @IBOutlet weak var backButtonIsPressed: UIButton!
    
    let x = "X"
    let o = "O"
    let yourMoveText = "Your Move"
    let computerMoveText = "Opponent is making move"
    
    var wins = [[1,2,3], [4,5,6], [7,8,9], [1,4,7], [2,5,8], [3,6,9], [1,5,9], [3,5,7]]
    var playerMoves:[Int] = []
    var computerMoves:[Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentMoveLabel.text = yourMoveText
        customizeButtons()
    }
    
    @IBAction func backButtonIsPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func optionButtonIsPressed(_ sender: UIButton) {
        sender.setTitle(x, for: .normal)
        playerMoves.append(sender.tag)
        sender.isUserInteractionEnabled = false
        var index = 0
        for element in optionButtons {
            if sender == element {
                optionButtons.remove(at: index)
            }
            index += 1
        }
        self.view.isUserInteractionEnabled = false
        checkWin(moves: playerMoves, name: "Player")
        computerMove()
    }
    
    func computerMove() {
        if optionButtons.count > 0 {
            currentMoveLabel.text = computerMoveText
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                let range = self.optionButtons.count - 1
                let randomInt = Int.random(in: 0...range)
                self.computerMoves.append(self.optionButtons[randomInt].tag)
                self.optionButtons[randomInt].setTitle(self.o, for: .normal)
                self.optionButtons[randomInt].isUserInteractionEnabled = false
                self.optionButtons.remove(at: randomInt)
                self.checkWin(moves: self.computerMoves, name: "Opponent")
                self.currentMoveLabel.text = self.yourMoveText
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    func checkWin(moves: [Int], name: String) {
        if moves.count >= 3 {
            for win in wins {
                var winConditionCounter = 0
                for element in win {
                    for move in moves {
                        if element == move {
                            winConditionCounter += 1
                        }
                    }
                }
                if winConditionCounter == 3 {
                    presentAlertController(name: name)
                }
            }
        }
    }
    
    func presentAlertController(name: String) {
        let alert = UIAlertController(title: nil, message: "\(name) WIN!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (_) in
            self.dismiss(animated: true)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    func customizeButtons() {
        for button in optionButtons {
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.black.cgColor
        }
    }
    
}
