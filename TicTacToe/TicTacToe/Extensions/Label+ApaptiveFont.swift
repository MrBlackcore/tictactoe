import UIKit

extension UILabel {
    
    func adaptFontSize(to height: CGFloat) {
        font = font.withSize(height*0.4)
    }
    
}
